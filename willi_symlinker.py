#!/usr/bin/env python
# -*- coding: utf8 -*-

import os
from glob import glob

renameFilter = lambda s: s.replace('_org', '_link')  # Change name of symlink

basepath = '.'
linkDestination = './foo_linked'

# Create linkDestination if it doesn't exist – because git might not be able
# to store empty directories
if not os.path.isdir(linkDestination):
    os.mkdir(linkDestination)

# Check that linkDestination is an empty directory
if os.listdir(linkDestination):
    print('\nWarning!',
          '\tDestination dir is not empty.',
          '\tTrying to create a symlink to an',
          '\t"occupied" path will crash the script.', end='\n\n', sep='\n')

globPattern = '**/*_symlink*.txt'  # only find txt files, '**'-pattern requires python 3.5+
print('Searching for txt-files in {}:'.format(os.path.abspath(basepath)), end='\n\n')
foundFiles = glob(globPattern, recursive=True)  # Perform file search
for fp in foundFiles:
    print(fp)
print('')  # add bælank line in terminal

# Do the symlinking
for fp in foundFiles:
    filename = os.path.split(fp)[-1]
    fpAbsPath = os.path.abspath(fp)
    # Could also do: linkPath = linkDestination + '/' + renameFilter(filename) but this
    # is more "pythnoic" and will also work in Windåze, if Windåze supports symlinks
    linkPath = (os.path.join(linkDestination, renameFilter(filename)))
    print("Symlinking the file '{originalPath}' to {linkPath}".format(  # continued below
          originalPath=fpAbsPath, linkPath=linkPath))
    os.symlink(fpAbsPath, linkPath)  # make the actual symlink



